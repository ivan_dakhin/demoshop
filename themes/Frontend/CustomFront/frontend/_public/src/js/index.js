$.overridePlugin('swSearch', {
    showResult: function() {
        var me = this;

        me.superclass.showResult.apply(this, arguments);

        me.$searchField.parents('form').css('z-index', 9998);
        me.$results.css('z-index', 9999);

        $.overlay.open();
    },

    closeResult: function() {
        var me = this;
        me.superclass.closeResult.apply(this, arguments);

        me.$searchField.parents('form').removeAttr('style');
        me.$results.removeAttr('style');

        $.overlay.close();
    }
});

$.overridePlugin('swEmotion', {
    init: function() {
        var me = this;

        me.superclass.init.apply(this, arguments);

        me.$container.addClass('test-custom-class');

        $.overlay.open();
    },
});

$('#modalOpenVideo').on('click', function () {
    $.modal.open('http://www.youtube.com/embed/5dxVfU-yerQ',
    {mode: 'iframe'})
})

$('#modalOpenForm').on('click', function () {
    $.modal.open(
        `<form id="testForm">
            <input type="text" placeholder="Text Input">
            <input type="search" placeholder="Search Input">
            <input type="password" placeholder="Password Input">
            <input type="number" placeholder="Number Input">
            <input type="email" placeholder="E-Mail Input">
            <input type="tel" placeholder="Phone Input">
            <button type="submit">Submit</button>
        </form>`,
        {title: 'My title',}
    )
})

$(document).ready( function() { // Wait until document is fully parsed
    $("#testForm").on('submit', function(e){

        e.preventDefault();
        console.log('submit');
    });
});


$('#modalOpenText').on('click', function () {
    $.modal.open('Hello World', {
        title: 'My title',
    })
})