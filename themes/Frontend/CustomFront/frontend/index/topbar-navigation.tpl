{extends file="parent:frontend/index/topbar-navigation.tpl"}

{block name="frontend_index_checkout_actions_service_menu"}
    {$smarty.block.parent}
    <a id="modalOpenVideo" class="btn is--custom">Custom Button Video</a>
    <a id="modalOpenForm" class="btn is--custom">Custom Button From</a>
    <a id="modalOpenText" class="btn is--custom">Custom Button Text</a>
{/block}