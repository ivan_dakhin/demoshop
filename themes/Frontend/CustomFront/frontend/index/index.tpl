{extends file="parent:frontend/index/index.tpl"}

{block name="frontend_index_page_wrap"}
    {$smarty.block.parent}
    <p>Logo Text</p>
{/block}

{block name="frontend_index_content_main_classes"}

    {if !$hasEmotion}
        {$smarty.block.parent} container-custom-background
    {else}
        {$smarty.block.parent}
    {/if}

{/block}


