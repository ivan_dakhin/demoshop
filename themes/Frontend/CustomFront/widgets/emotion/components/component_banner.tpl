{extends file="parent:widgets/emotion/components/component_banner.tpl"}

{block name="widget_emotion_component_banner"}
    {debug}

    {if $element.component.template == "component_banner"}
        <div class="emotion--banner custom-banner-background"
             data-coverImage="true"
             data-width="{$Data.fileInfo.width}"
             data-height="{$Data.fileInfo.height}"
             {if $Data.bannerMapping}data-bannerMapping="true"{/if}>

            {block name="widget_emotion_component_banner_inner"} {$smarty.block.parent} {/block}
        </div>
    {else}
        {$smarty.block.parent}
    {/if}

{/block}

